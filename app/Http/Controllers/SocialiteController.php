<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Socialite as Sosial;
use App\User;

class SocialiteController extends Controller
{
    public function redirectSocialite($socialite)
    {
        return Socialite::driver($socialite)->redirect();
    }

    public function callback($socialite)
    {
        try {
            $user = Socialite::driver($socialite)->user();
        } catch (\Exception $e) {
            return redirect(route('login'));
        }

        $auth = $this->findOrCreate($socialite, $user);

        Auth::login($auth, true);

        return redirect('/home');
    }

    public function findOrCreate($socialite, $user)
    {
        $sosial = Sosial::where('socialite_id', $user->getId())->where('socialite_type', $socialite)->first();

        if ($sosial) {
            return $sosial->user;
        } else {


            $d = User::where('email', $user->getEmail())->first();

            if (!$d) {
                $d = User::create([
                    'name' => $user->getName(),
                    'email' => $user->getEmail()
                ]);
            }

            $d->socialite()->create([
                'socialite_id' => $user->getId(),
                'socialite_type' => $socialite
            ]);

            return $d;
        }
    }
}
